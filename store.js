//To include the File System module
var fs = require('fs');

//to include the Path module
var path = require('path');

// to read database file
var file_path = path.join(__dirname,"data.db");

console.log("file path is "+file_path)
console.log("process argv "+process.argv);

//check if the file exist
if(fs.existsSync(file_path)){

  //read database file in json format
  let dictionary = JSON.parse(fs.readFileSync(file_path));

  //check the paramater which is after 'node store.js' 
  switch (process.argv[2]) {

    //if the paramater is "list" print the dictionary (key , value store) 
    case "list" : console.log(dictionary);
      break;

    //if the paramater is "add" add to dictionary (key , value) and if the user add only key set it's value as ""   
    case "add" :  dictionary[process.argv[3]] = process.argv[4] || "";
      break;

    //if the paramater is "get" print the value o the key that user entered   
    case "get" : console.log(dictionary[process.argv[3]]);
      break;

    //if the paramater is "remove" remove the key and value that the user entered   
    case "remove" :   delete(dictionary[process.argv[3]]);
      break;

    //if the paramater is "clear" remove all the dictionary  
    case "clear" : dictionary = {};
        break;
    default:
        console.log("Please enter an input");
  }

  //write on file, but synchronous
  fs.writeFileSync(file_path,JSON.stringify(dictionary));
}else{
  console.log("file not found");
}

