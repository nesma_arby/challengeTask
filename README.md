### Store Commands

`$ node store.js add mykey myvalue`

`$ node store.js list`

`$ node store.js get mykey`

`$ node store.js remove mykey`

`$ node store.js clear`

### Bonus

- Write clean, modular and testable code.
- Instead of running `node store.js` alter the runtime so it can be run as `./store`.
- Add ability to deploy in Docker container.
- Add GitLab compatible CI/CD to perform a server deploy.


# Nesma's comments

# if you want to run as ./store instead of node store.js you should follow these steps
1- write in cmd `$ echo node store.js "the action that you need (list or add key value or remove key or clear)" > ./store`
 for example `$ echo node store.js list > ./store`
2- run `$ ./store`

# References

# https://nodejs.org/en/docs/guides/nodejs-docker-webapp/



